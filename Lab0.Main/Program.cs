﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class Wegiel
    {
        private int twardosc;
        private string kolor;
        public Wegiel()
        {
            this.twardosc = 1;
            this.kolor = "Czarny";
        }

        public int Twardosc
        {
            get
            {
                return twardosc;
            }
            set
            {
                twardosc = value;
            }
        }
        public string Kolor
        {
            get
            {
                return kolor;
            }
            set
            {
                kolor = value;
            }
        }

        public virtual string Informacja()
        {
            return ("Podstawowy typ węgla o kolorze: " + kolor + "i twardości: " + twardosc);
        }
    }

    class Diament : Wegiel
    {
        public Diament()
        {
            this.Twardosc = 5;
            this.Kolor = "Diamentowy";
        }
        public override string Informacja()
        {
            return ("Najtwardszy rodzaj węgla o kolorze: " + Kolor + " i  twardości: " + Twardosc);
        }
    }

    public class Grafit : Wegiel
    {
        public Grafit()
        {
            this.Twardosc = 3;
            this.Kolor = "szary";
        }
        public override string Informacja()
        {
            return ("Twardy rodzaj węgla o kolorze: " + Kolor + "i twardości: " + Twardosc);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Wegiel w1 = new Wegiel();
            Diament d1 = new Diament();
            Grafit g1 = new Grafit();
            Console.WriteLine(w1.Informacja());
            Console.WriteLine(d1.Informacja());
            Console.WriteLine(g1.Informacja());
            List<Wegiel> lista = new List<Wegiel>();
            lista.Add(w1);
            lista.Add(d1);
            lista.Add(g1);

            foreach (Wegiel item in lista)
            {
                Console.WriteLine(item.Twardosc);
                
            }
            Console.ReadKey();
        }
    }
}
