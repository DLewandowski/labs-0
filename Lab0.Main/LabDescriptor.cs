﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Wegiel);
        public static Type B = typeof(Diament);
        public static Type C = typeof(Grafit);

        public static string commonMethodName = "Informacja";
    }
}
